<?php

/**
 * Implements hook_queue_class_info.
 *
 * @return array of class information definitions.
 */
function tagged_systemqueue_queue_class_info() {
  return array(
    'TaggedSystemQueue' => array(
      'title' => t('Tagged System Queue (database Queue with tagged queries)'),
      'inspect' => TRUE,
      'operations' => array(
        'view',
        'release',
        'delete',
        'deleteall',
      ),
    ),
  );
}

class QueueUITaggedSystemQueue extends QueueUISystemQueue {}
